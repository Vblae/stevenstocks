CREATE TABLE Employee (
	SSN INT,
  	LastName CHAR(20) NOT NULL,
  	FirstName CHAR(20) NOT NULL,
  	Address CHAR(20) NOT NULL,
  	City CHAR(20) NOT NULL,
  	State CHAR(20) NOT NULL,
  	ZipCode INT,
	Telephone LONG,
  	StartDate DATE,
  	HourlyRate DOUBLE,
  	Role CHAR(20) CHECK (VALUE IN('Manager', 'Employee')),
  
  	PRIMARY KEY(SSN)
);

CREATE TABLE Client_ (
	ID INT CHECK (ID > 0 AND ID < 1000000000),
  	LastName CHAR(20) NOT NULL,
  	FirstName CHAR(20) NOT NULL,
  	Address CHAR(20) NOT NULL,
  	City CHAR(20) NOT NULL,
  	State CHAR(20) NOT NULL,
  	ZipCode INT,
	Telephone LONG,
  	Email CHAR(48),
  	CreditCard LONG,
  	Rating INT,
  
  	PRIMARY KEY(ID)
);

CREATE TABLE Account_(
	AccountNumber INT CHECK(AccountNumber > 0),
  	ClientID INT,
  	CreationDate DATE,
  
  	PRIMARY KEY (AccountNumber),
  
  	FOREIGN KEY (ClientID) REFERENCES Client_(ID)
  		ON DELETE CASCADE
  		ON UPDATE CASCADE
);

CREATE TABLE HasAccount(
	ClientID INT,
  	AccountNumber INT,
  
  	PRIMARY KEY (ClientID, AccountNumber),
  
  	FOREIGN KEY (ClientID) REFERENCES Client_(ID)
  		ON DELETE CASCADE
  		ON UPDATE CASCADE,
  	FOREIGN KEY (AccountNumber) REFERENCES Account_(AccountNumber)
  		ON DELETE CASCADE
  		ON UPDATE CASCADE
);

CREATE TABLE StockType(
	Type CHAR(24),
    
   	PRIMARY KEY (Type)
);

CREATE TABLE Stock(
	StockSymbol CHAR(5),
  	CompanyName CHAR(32) NOT NULL,
  	Type CHAR(20) NOT NULL,
  	PricePerShare DOUBLE,
   	NumShares INT,
  
  	PRIMARY KEY (StockSymbol),
    
	FOREIGN KEY (Type) REFERENCES StockType(Type)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE HasStock(
	AccountNumber INT,
  	Symbol CHAR(5),
  	NumShares INT,
  
  	PRIMARY KEY (AccountNumber, Symbol),
  
  	FOREIGN KEY (AccountNumber) REFERENCES Account_(AccountNumber)
  		ON DELETE CASCADE
  		ON UPDATE CASCADE,
  	FOREIGN KEY (Symbol) REFERENCES Stock(StockSymbol)
  		ON DELETE CASCADE
  		ON UPDATE CASCADE
);

CREATE TABLE Order_(
  	OrderNumber INT CHECK(OrderNumber > 0),
  	NumberShares INT CHECK(NumberShares > 0),
  	PricePerShare DOUBLE,
	StockSymbol CHAR(5) NOT NULL,
  	DateTime	DATETIME,
  	OrderType CHAR(5) CHECK(VALUE IN ('Buy', 'Sell')),
	PriceType CHAR(20) CHECK(VALUE IN ('Market', 'MarketOnClose', 'TrailingStop', 'HiddenStop')),
  
  	PRIMARY KEY (OrderNumber),
    
	FOREIGN KEY (StockSymbol) REFERENCES Stock(StockSymbol)
		ON DELETE NO ACTION
		ON UPDATE CASCADE
);

CREATE TABLE Transaction_(
	TransactionID INT CHECK(TransactionID > 0),
  	Fee DOUBLE,
  	PricePerShare DOUBLE,
  	DateTime DATETIME,
  
  	PRIMARY KEY (TransactionID)
);

CREATE TABLE Trade(
	AccountNumber INT,
  	OrderNumber INT,
  	TransactionID INT,
  	EmployeeID INT,	
  
  	PRIMARY KEY (AccountNumber, OrderNumber, TransactionID),
  
  	FOREIGN KEY (AccountNumber) REFERENCES Account_(AccountNumber)
  		ON DELETE NO ACTION
  		ON UPDATE CASCADE,
  	FOREIGN KEY (OrderNumber) REFERENCES Order_(OrderNumber)
  		ON DELETE NO ACTION
  		ON UPDATE CASCADE,
  	FOREIGN KEY (TransactionID) REFERENCES Transaction_(TransactionID)
  		ON DELETE NO ACTION
  		ON UPDATE CASCADE,
  	FOREIGN KEY (EmployeeID) REFERENCES Employee(SSN)
  		ON DELETE NO ACTION
  		ON UPDATE CASCADE
);

CREATE TABLE StockHistory(
	StockSymbol CHAR(5),
   	PricePerShare DOUBLE,
	DateTime DATETIME,
    
	PRIMARY KEY (StockSymbol, DateTime),
	FOREIGN KEY (StockSymbol) REFERENCES Stock(StockSymbol)
);

CREATE TABLE OrderHistory(
	OrderNumber INT,
	PricePerShare DOUBLE,
	DateTime DATETIME,

	PRIMARY KEY (OrderNumber, DateTime),
	FOREIGN KEY (OrderNumber) REFERENCES Order_(OrderNumber)
);
