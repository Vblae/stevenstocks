INSERT INTO employee 
VALUES(222333444,'Bryant','Kobe','Staples Center','Los Angeles','CA',90015,'1234567890','1996-06-26',5000,'Manager','mvp','kobe');

INSERT INTO employee
VALUES(123456789,'McD','Ron','100 OP Street','New York','NY',12345,'1231231234','2016-08-14',50,'Employee','pass','ronald');

INSERT INTO stock
VALUES('AMD','AM Devices','computer',63.12,400);

INSERT INTO stock
VALUES('F','Ford','automotive',8.5,850);

INSERT INTO stock
VALUES('GM','General Motors','automotive',34.23,1000);

INSERT INTO stock
VALUES('IBM','IBM','computer',91.41,500);

INSERT INTO stock
VALUES('S','Sprint','communications',34.34,1000);

INSERT INTO stock
VALUES('VZ','Verizon','communications',45.23,1000);

INSERT INTO stock
VALUES('ATT','AT&T','communications',98.23,1000);

INSERT INTO stock
VALUES('TM','T-Mobile','communications',78.34,1000);